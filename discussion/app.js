// Setting up dependencies
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

// Access to routes
const CourseRoute = require('./routes/courseRoute')
const UserRoute = require('./routes/userRoute')

// Server
const app = express(), port = 4000
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
// Allows origins/domains to access the backend application
app.use(cors())

// Database
mongoose.connect("mongodb+srv://jhnnycyn:dxtr008X@cluster0.z5eip.mongodb.net/b170-course-booking?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

// Defines the routes where CRUD operation will be performed
app.use('/api/users', UserRoute)
app.use('/api/courses', CourseRoute)
app.listen(port, () => console.log(`API now online at port:${port}`))
