const User = require('./../models/user')
const Course = require('./../models/course')
const auth = require('./../auth')
// Used to encypt user's password
const bcrypt = require('bcrypt')

module.exports.checkEmail = requestBody => User
    .find({ email: requestBody.email })
    .then((result, error) => error ? false : result.length > 0)

module.exports.registerUser = requestBody => {
    const newUser = new User({...requestBody})
    newUser.password = bcrypt.hashSync(newUser.password, 10)
    return newUser.save().then((success, error) => !error)
}

module.exports.loginUser = requestBody => {
    return User.findOne({email: requestBody.email}).then(result => {
        if (result) 
            return bcrypt.compareSync(requestBody.password, result.password) ? 
            { access: auth.createAccessToken(result.toObject()) } : false
        else 
            return false
    })
}


module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		if (result === null) {
			return false
		} else {
			result.password = "";
			return result
		}
	})
}

// S43 Activity
// module.exports.enrollCourse = data => {
//     const { id: userId, isAdmin, ...course } = data
//     return User.findById(userId).then((result, error) => {
//         if (!result || isAdmin) 
//             return false
//         else {
//             // Admin cannot enroll in any course.
//             result.enrollment.push(course)
//             return result.save().then((success, error) => !error)
//         }
//     })
// }

// S36 Activity
module.exports.enrollCourse = async data => {
    const { id: userId, isAdmin, ...course } = data
    let isUserUpdated = await User.findById(userId).then(user => {
        user.enrollment.push(course)
        return user.save().then((success, error) => !error)
    })

    isUserUpdated = await Course.findById(course.courseId).then(course => {
        course.enrollees.push({ userId: userId })
        return course.save().then((success, error) => !error)
    })

    return isUserUpdated
}