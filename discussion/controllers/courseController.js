const Course = require('./../models/course')
const auth = require('./../auth')
const bcrypt = require('bcrypt')
const User = require('../models/user')

module.exports.addCourse = data => {
    const { isAdmin, id: userId, ...course } = data
    
    return User.findById(userId).then(result => {
        if (!isAdmin)
            return false
        else {
            const newCourse = new Course(course)
            return newCourse.save().then((success, error) => isAdmin ? !error : false)
        }
    })    
}

module.exports.getAllCourses = () => Course.find().then(result => result)
module.exports.archiveCourse = data => {
    const { isAdmin, id: userId, courseId } = data
    
    return User.findById(userId).then(result => {
        if (!isAdmin)
            return false
        else {
            return Course.findById(courseId).then((result, error) => {
                result.isActive = false
                return result.save().then((success, error) => !error)
            })   
        }
    }) 
}

module.exports.getSpecificCourse = id => Course.findById(id).then(result => result)
module.exports.getActiveCourses = () => Course.find({ isActive: true }).then(result => result)
module.exports.updateCourse = (params, body) => {
	let updatedCourse = {
		name: body.name,
		description: body.description,
		price: body.price
	}
	
	return Course.findByIdAndUpdate(params.id, updatedCourse).then((result, error) => !error)
}
