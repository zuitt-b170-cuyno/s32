const jwt = require('jsonwebtoken')
const secret = "secret"

// JSON Web Token - securely passes information from server to client
// Information is kept secured through the use of variable called secret
// The variable secret is known only by the system/browser which can be decoded by the user

// Token creation
module.exports.createAccessToken = user => {
    // Data will be recieved from the registration form
    // When the user login, a token will be created with the user's information 
    // The information will be encrypted inside the token
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    // Generates the token using the form data and the secret data without additional options
    return jwt.sign(data, secret, {})
}

// The parameter next tells the server to proceed if the verification is successful
// Authorization can be found in the header of a request and tell whether the client/user has the authority to send the request
// jwt.verify(...) verifies the token using the secret and fails if the token's secret does not match the secret variable
module.exports.verify = (req, res, next) => { 
    let token = req.headers.authorization

	if (typeof token !== "undefined") { 
		console.log(token)
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (error, data) => {
			if (error) 
				return res.send({ auth: "failed" })
			else 
				next()
		})
	}
}

// jwt.decode(...) decides the data to be decoded which is the token
// payload is the one that we need to verify the user information 
// payload is part of the token when code the createAcessToken(...), the one with _id, email, isAdmin
module.exports.decode = (token) => {
	if (typeof token !== "undefined") { 
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				console.log(err)
				return null
			} else 
				return jwt.decode(token, {complete: true }).payload
		})
	} 
    else 
		return null
}
