const express = require('express')
const UserController = require('./../controllers/userController')
const router = express.Router()
const auth = require('./../auth')

router.post('/checkEmail', (req, res) => UserController
    .checkEmail(req.body)
    .then(result => res.send(result)))

router.post('/register', (req, res) => UserController
    .registerUser(req.body)
    .then(result => res.send(result)))

router.post('/login', (req, res) => UserController
    .loginUser(req.body)
    .then(result => res.send(result)))

// auth.verify ensures that a user is logged in before proceeding to the next part of the code
// decode decrypts the token inside the authorization (which is in the headers of the request) 
// req.headers.authorization contains the token that was created for the user
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	UserController.getProfile({ userId: userData.id }).then(result => res.send(result))
})

// S34 Activity
router.post('/enrolls', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    UserController.enrollCourse({ ...userData, ...req.body }).then(result => res.send(result))
}) 

module.exports = router
