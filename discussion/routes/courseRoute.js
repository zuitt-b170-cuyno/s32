const express = require('express')
const CourseController = require('./../controllers/courseController')
const router = express.Router()
const auth = require('./../auth')

// S43 Activity
router.post('/', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    CourseController.addCourse({ ...userData, ...req.body }).then(result => res.send(result))
})

router.get("/", (req, res) => CourseController.getAllCourses().then(result => res.send(result)))
router.get("/active", (req, res) => CourseController.getActiveCourses().then(result => res.send(result)))

router.put('/archiveCourse', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    CourseController.archiveCourse({ ...userData, courseId: req.body.id }).then(result => res.send(result))
})

// Sir's improved code
router.put('/archive/:id', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    CourseController.archiveCourse({ ...userData, courseId: req.params.id }).then(result => res.send(result))
})

router.get("/:id", (req, res) => CourseController.getSpecificCourse(req.params.id).then(result => res.send(result)))
router.put("/:id", auth.verify, (req, res) => CourseController.updateCourse(req.params, req.body).then(result => res.send(result)))

module.exports = router
